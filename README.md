# Start server, deploy application, monitoring server and app

1. [preparing servers](guids/preparing_servers.md)
2. [install grafana](https://grafana.com/docs/grafana/latest/installation/debian/)
3. [install prometheus](guids/prometheus.md)
4. [install exporter](guids/exporter.md)
5. [install loki](https://grafana.com/docs/loki/latest/installation/) and [config loki](config/loki/loki.yml) 
