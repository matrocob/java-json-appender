    # Node exporter


## steps

1) install

```
wget https://github.com/prometheus/node_exporter/releases/download/v1.2.0/node_exporter-1.2.0.linux-amd64.tar.gz
tar xvfz node_exporter-1.2.0.linux-amd64.tar.gz
rm -r node_exporter-1.2.0.linux-amd64.tar.gz
cd node_exporter-1.2.0.linux-amd64/
./node_exporter
```

2) import in grafana

https://grafana.com/grafana/dashboards/1860