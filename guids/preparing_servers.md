# preparing servers

0) copy your ssh and input pwd (password)

```
ssh-copy-id root@{ip}
```   

1) login via ssh root@{ip}
2) configure firewall

2.1) [copy-docker-ufw-script](https://github.com/chaifeng/ufw-docker#solving-ufw-and-docker-issues)

2.2) change  **sudo nano /etc/ufw/after.rules**

2.3) run this command
```
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow ssh
sudo ufw allow from 10.0.0.0/16
sudo ufw route allow proto tcp from 10.0.0.0/16
sudo ufw route allow proto tcp from 135.181.145.62
sudo ufw enable
sudo systemctl restart ufw
```

3) create user with pwd 1q2w3e4r

```
adduser apps
```
add role
```
usermod -aG sudo apps
```

4) reconnect;  logout - ctrl + A + D; configure permission to login via ssh.
```
sudo nano /etc/ssh/sshd_config
```

Change 
```
PermitRootLogin no
PasswordAuthentication no
AllowUsers root apps
```

5) restart
```
sudo systemctl restart ssh.service
```

P.S.: added new user with public key
```
echo "ssh-rsa someKey" >> /root/.ssh/authorized_keys
```

6) [ENABLE SWAP](https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-20-04)