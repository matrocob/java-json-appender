# install prometheus

0) https://prometheus.io/docs/prometheus/latest/getting_started/)
1) release https://prometheus.io/download/ 
2) install ubuntu
```
wget https://github.com/prometheus/prometheus/releases/download/v2.28.1/prometheus-2.28.1.linux-amd64.tar.gz
mkdir prometheus
cd prometheus
tar -xvzf ~/prometheus-2.25.0.linux-amd64.tar.gz --strip 1
rm -r ~/prometheus-2.25.0.linux-amd64.tar.gz 
```
3) configure [prometheus.yml](config/prometheus/prometheus.yml)
