package org.apps.log4j2.json.appender;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProxyException {

  private String name;
  private String message;
  private String localizedMessage;
  private String detailMessage;
  private StackTraceElement[] stackTrace;

  public static ProxyException of(Throwable throwable) {
    return ProxyException.builder()
        .name(throwable.getClass().getName())
        .message(throwable.getMessage())
        .localizedMessage(throwable.getLocalizedMessage())
        .stackTrace(throwable.getStackTrace())
        .build();
  }
}
