/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache license, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */
package org.apps.log4j2.json.appender;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.lookup.AbstractLookup;
import org.apache.logging.log4j.core.lookup.StrLookup;
import org.apache.logging.log4j.status.StatusLogger;

import java.io.IOException;
import java.util.Properties;

/**
 * Looks up keys from gos-log4j.properties file.
 */
@Plugin(name = "apps", category = StrLookup.CATEGORY)
public class AppsLogPropertiesLookup extends AbstractLookup {

  private static final Logger LOGGER = StatusLogger.getLogger();
  private static final Marker LOOKUP = MarkerManager.getMarker("APP-NAME-LOOKUP");
  private Properties LogProperties = null;


  public AppsLogPropertiesLookup() {
  }

  /**
   * Looks up the value for the key using the data in the LogEvent.
   *
   * @param event The current LogEvent.
   * @param key   the key to be looked up, may be null
   * @return The value associated with the key.
   */
  @Override
  public String lookup(final LogEvent event, final String key) {

    if (LogProperties == null) {
      try {
        var props2 = loadServiceProps();

        LogProperties = new Properties();
        LogProperties.putAll(props2);

      } catch (Exception ex) {
        ex.printStackTrace();
        LOGGER.warn(LOOKUP, "Error while getting Gosloto property [{}].", key, ex);
      }

    }

    var value = LogProperties.get(key);
    if (value == null) {

      if ("fileName".equals(key)) {
        return System.getProperty("user.home") + "/logs/" + LogProperties.get("appName") + "/log.log";
      }

      return key;
    }

    return value.toString();
  }


  private Properties loadServiceProps() throws IOException {
    var is = getClass().getClassLoader().getResourceAsStream("apps-log4j.properties");
    LogProperties = new Properties();
    LogProperties.load(is);
    return LogProperties;
  }
}
