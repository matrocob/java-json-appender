package org.apps.api.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
public class MockController {

  @GetMapping("/mock")
  public Map<String, String> mock() {
    log.info("mock: test");
    return Map.of("status", "ok");
  }
}
